# iHouzz

iHouzz is an open source intelligent house application. As a front-end it uses the CHC-web application available on this gitlab also as open source software. For more information please read README fil

building project:
 Build system is based on makefile mechanism.
 To build the project you can use one of 2 recomended methods.

1. Build one app
To build particular application just type: make <app_name> i.e. make iHouzz
2. Build all apps
 It is standard target used in many projects. If You don't want to think about which app should I build, simply type make all and thats it

# !!! File top_secret.h !!!
This file is in .gitignore, and shoud never be pushed to master!
It is the file containing all data, we don't want to sheare with world wide (passwords, and so on).
If Your project won't compile (and c/cpp file includes this file), look if top_secret.h is created, 
If so, check if particular definition should be saved in this file and if yes, check if this definition
is there. Example implemetnation:

```c
#ifndef TOP_SECRET_H
#define TOP_SECRET_H

#define SQL_USR  "web"
#define SQL_PASS "somepassword"
#define SQL_DB   "iHouzz"
#define SQL_ADDR "127.0.0.1"

int generate_key () {
	return 0xF45B24;
}

#endif //#ifndef TOP_SECRET_H
```


