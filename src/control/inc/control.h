#ifndef CONTROL_H
#define CONTROL_H

#include <vector>
#include <thread>

#include "node.h"
#include "node1Wire.h"
#include "nodeMqtt.h"

class Control {
public:
	Control();
	void run();
	
	void set_T_set( float T_set_value ) {
	    T_set = T_set_value;
	    T_min = T_set - histeresis;
	    T_max = T_set + histeresis;
	}
	
	void set_histeresis( float histeresis_value ) {
	    histeresis = histeresis_value;
	    T_min = T_set - histeresis;
	    T_max = T_set + histeresis;
	}

    float get_T_set() const {
        return T_set;
    }
    
    float get_histeresis() const {
        return histeresis;
    }
    
private:

	float T_set;
	float histeresis;

	float T_min;
	float T_max;

	std::vector<std::unique_ptr<Node>> tempSensors;//TODO: Just node instead concrete nodeType
	NodeMqtt nodeOnOff = NodeMqtt("HEATER_OnOff");

	enum State {
		HEATING,
		CHILLING,
		INIT,
	};

	State state;
};

#endif //#ifndef CONTROL_H
