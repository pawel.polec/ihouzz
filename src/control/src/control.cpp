#include "control.h"

#include <iostream>
#include <thread>
#include <exception>

Control::Control() : T_set{22.25}, histeresis{0.25} { //TODO: Load this values from DB...
	std::vector<std::string> devList = oneWire::getDevList();

	for (auto &device : devList) // access by reference to avoid copying
	{
	    if (oneWire::getDevType (device) & oneWire::OW_TERMOMETER != 0) {
	    	tempSensors.push_back(std::make_unique<Node1Wire>(device));
	    	std::cout<<"ADDED: "<< device<<'\n';
	    }
	}
 
	nodeOnOff.setWriteTopic("iHouzz/modules/onoff/set/state");

	T_min = T_set - histeresis;
	T_max = T_set + histeresis;

	state = INIT;
}

void Control::run() {
	float T_mid = 0;
	if (! tempSensors.empty()) {
		for (const auto& device : tempSensors) // why &&? Because lval (const auto& is rval). Lern more
		{
			try
			{
				T_mid += device->readValue();
			}
			catch (std::exception& e)
			{
				std::cout << e.what() << '\n';
			}
		}
		T_mid /= tempSensors.size();
	}
	std::cout<<"T_mid = "<< T_mid<<'\n';

	switch (state) {
	case CHILLING:
		if (T_mid <= T_min) {
			nodeOnOff.writeValue("on");
			state = HEATING;
		}
		break;

	case HEATING:
		if (T_mid >= T_max) {
			nodeOnOff.writeValue("off");
			state = CHILLING;
		}
		break;

	case INIT:
		if (T_mid <= T_min) {
			nodeOnOff.writeValue("on");
			state = HEATING;
		} else if (T_mid >= T_max) {
			nodeOnOff.writeValue("off");
			state = CHILLING;
		}
		break;

	default:
		break;
	}
}
