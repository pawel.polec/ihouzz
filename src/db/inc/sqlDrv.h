#ifndef SQL_DRV_H
#define SQL_DRV_H

#include "observer.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

namespace DB {

class SqlObj : Subject {

};

class SqlDb {
	public:

	SqlDb() : con(nullptr), stmt(nullptr), res(nullptr) {
		driver = get_driver_instance();
	}

	~SqlDb() {
		delete res;
		delete stmt;
		delete con;
	}

	bool connect();
	sql::ResultSet* runQuery(const sql::SQLString& query);

	private:
	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::ResultSet *res;
};

}

#endif
