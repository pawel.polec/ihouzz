#include "sqlDrv.h"

#include "top_secret.h"

namespace DB {

bool SqlDb::connect() {
	if (!con && driver) {
		con = driver->connect(SQL_ADDR, SQL_USR, SQL_PASS);
	}
	return true;
}

sql::ResultSet* SqlDb::runQuery(const sql::SQLString& query) {
	connect();
	
	if (!stmt) stmt = con->createStatement();
	if (res) delete res;
	res = stmt->executeQuery(query);
	return res;
}

}

	// try {

		// /* Create a connection */
		// con = driver->connect(SQL_ADDR, SQL_USR, SQL_PASS);
		// /* Connect to the MySQL test database */
		// con->setSchema(SQL_DB);

		// stmt = con->createStatement();
		// res = stmt->executeQuery("SELECT * FROM loggedTemps LIMIT 10");
		
		// while (res->next()) {
			// /* Access column data by alias or column name */
			// cout << res->getDouble("temp") << endl;
		// }

	// } catch (sql::SQLException &e) {
		// cout << "# ERR: SQLException in " << __FILE__;
		// cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		// cout << "# ERR: " << e.what();
		// cout << " (MySQL error code: " << e.getErrorCode();
		// cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	// }