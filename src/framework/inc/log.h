#ifndef _LOG_H
#define _LOG_H

#include <chrono>
#include <iostream>
#include <string>

#include <ctime>

#define GET_TTP() std::time_t ttp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())
#define PREFIX "--- "

class log {
public:
	template <typename T>
    static void console (T value);

	template <typename T>
    static void console (T value, std::string name);

    static void console (std::string text);
};

template <typename T>
void log::console (T value) {
	GET_TTP();
	std::cout << std::ctime(&ttp) << PREFIX << value << std::endl;
}

template <typename T>
void log::console (T value, std::string name) {

	GET_TTP();
	std::cout << std::ctime(&ttp) << PREFIX << name << "="<< value << std::endl;
}


#endif //_LOG_H
