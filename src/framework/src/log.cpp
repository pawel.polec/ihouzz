#include "log.h"

void log::console (std::string text) {
	GET_TTP();
	std::cout << std::ctime(&ttp) << PREFIX << text << std::endl;
}
