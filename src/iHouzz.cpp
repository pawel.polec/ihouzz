/*
 ============================================================================
 Name        : iHouzz.cpp
 Author      : StinG
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C++,
 ============================================================================
 */

#include <iostream>
#include "log.h"
#include <unistd.h>

#include "observer.h"
#include "settings.h"
#include "node1Wire.h"
#include "nodeMqtt.h"
#include "control.h"
#include "sqlDrv.h"


using namespace std;

class OB1 : public Observer{

	int dupa;
	public:

	virtual void update(void * data) {
		log::console(dupa, std::string("Dupa: "));
	}

	OB1(int d, class Subject &s): dupa(d) {s.Register(this);};
};

class OB2 : public Observer{

	int dupa;
	public:
	virtual void update(void * data) {
		log::console(dupa, std::string("Dupa2"));
	}

	OB2(int d, class Subject &s): dupa(d) {s.Register(this);};
};

int main(void) {

	// class Subject s;
	// class OB1 d1(1, s), d2(2, s), d3(3, s);

	// class OB2 d21(21, s), d22(22, s), d23(23, s);


	// s.Notify();
	// log::console(std::string("Try this"));

	// Settings *sss = Settings::getInstance();
	// log::console(std::string("ins ok"));

	// Subject* sub = sss->getChangedSubject();
	// log::console(std::string("sub ok"));
	// sub->Register(&d21);

	std::vector<std::string> devList = oneWire::getDevList();

	for (auto &device : devList) // access by reference to avoid copying
	{
	    std::cout<<device<<'\n';
	}

	if (!devList.empty()) {
		Node1Wire node1(devList[0]);
	}

	NodeMqtt nodeOnOff("Some-Stube");
	nodeOnOff.setWriteTopic("iHouzz/modules/onoff/set/state");
	
	
	nodeOnOff.setReadTopic("#");
	
	//log::console(node1.readValue(), std::string("Value name here: "));

	Control ctrl;
	
	DB::SqlDb db;


	while (1) {
		//log::console(node1.readValue(), std::string("Value name here: "));
		ctrl.run();
		sleep(5);
	};
	return 0;
}
/*
#include <iostream>
#include <charconv>
#include <system_error>
#include <string_view>
#include <array>

int main()
{
    std::array<char, 10> str;

    if(auto [p, ec] = std::to_chars(str.data(), str.data() + str.size(), 42);
       ec == std::errc())
        std::cout << std::string_view(str.data(), p - str.data());
}*/
