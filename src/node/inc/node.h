#ifndef NODE_H
#define NODE_H

#include<string>
#include<cmath>
#include <exception>


class NodeUnsupportedExc: public std::exception
{
  virtual const char* what() const throw()
  {
    return "Unsupported method in this class";
  }	
};

class Node {
protected:
	std::string m_id;
	std::string m_name;
	NodeUnsupportedExc unsuppex;
public:
	virtual int read () = 0;
	virtual int write () = 0;

	virtual float readValue () { throw unsuppex; }
};

#endif //NODE_H
