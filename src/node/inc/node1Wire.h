#ifndef NODE_1WIRE_H
#define NODE_1WIRE_H

#include "node.h"
#include <vector>


class Node1Wire: public Node {
//
//	std::string m_id;
//	std::string m_name;
public:
	Node1Wire(std::string id);

	virtual int read () {return 0;};
	virtual int write () {return 0;};

	virtual float readValue ();
};


namespace oneWire {
	std::vector<std::string> getDevList();
	
	enum DevType {
		OW_DEVICE_UNKNOWN = 0,
		OW_TERMOMETER     = 1<<0,
		OW_HUMIDITY       = 1<<1,
		OW_MEMORY         = 1<<2
	};
	
	unsigned int getDevType (std::string id);
}

#endif //NODE_MQTT_H
