#ifndef NODE_MQTT_H
#define NODE_MQTT_H

#include "node.h"
#include <mosquitto.h>
#include <thread>

class NodeMqtt: public Node {
public:
	virtual int read () {  return 0; };
	virtual int write () { return 0; };

	//Only temporary!
	void write (std::string value) {
		mosquitto_publish( mosq, NULL, writeTopic.c_str(), value.length(), value.c_str(), 1, true);
	}

	virtual float readValue ();
	virtual void writeValue ( std::string value ) { write(value); } ; //

	NodeMqtt (std::string id);
	NodeMqtt(NodeMqtt &&) = default;

    ~NodeMqtt(){
         stop_thread = true;
         if (mqtt_thread.joinable()) mqtt_thread.join();

         mosquitto_destroy(mosq);
    }

    void setReadTopic (std::string topic);
    void setWriteTopic (std::string topic);

private:
	static void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);
	void callback(struct mosquitto *mosq, const struct mosquitto_message *message);
	void mqtt_loop( );

	std::thread mqtt_thread;
	bool stop_thread = false;

	struct mosquitto *mosq;

	std::string writeTopic;
	std::string readTopic;
};

#endif //NODE_MQTT_H
