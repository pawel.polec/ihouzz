#include "node1Wire.h"
#include "oneWire.h"
#include <iostream>
#include <fstream>
//#include <experimental/filesystem>
#include <filesystem>
//namespace fs = std::experimental::filesystem;
namespace fs = std::filesystem;
Node1Wire::Node1Wire (std::string id) {
	m_id = id;

	//check if file exists?
}

float Node1Wire::readValue () {
	std::string path(ONEWIRE_PATH);
	path += m_id;
	path += ONEWIRE_SLAVE_DIR;

	//std::cout<<"read ["<<path<<"]\n";

	std::ifstream myfile (path);

	std::string line;
	std::string value = "0";

	if (myfile.is_open())
	{
		while ( std::getline (myfile, line) )
		{
			std::size_t found = line.find("t=");

			if (found!=std::string::npos)
			    value = line.substr(found+2);
		}
		myfile.close();
	} else {
		std::cout<<"ERROR!!!!\n";
	}
	//std::cout<<"val = " << value << '\n';

	return stof(value)/1000;
}

//using namespace oneWire;

std::vector<std::string> oneWire::getDevList()
{
	std::vector<std::string> ret;
	
	try {
		std::string path(ONEWIRE_PATH);

		for(const auto& p: fs::directory_iterator(path))
			if (p.path().filename().string()[2] == '-') { //validate if format is CC-LONG_ID (CC - device class)
				ret.push_back( p.path().filename().string() );
			}
	} catch (...) {
	}

	return ret;
}

unsigned int oneWire::getDevType (std::string id)
{
	unsigned int rc = OW_DEVICE_UNKNOWN;
	
	if (id.find("28-") !=  std::string::npos || id.find("10-") !=  std::string::npos || id.find("26-") !=  std::string::npos) {
		rc |= OW_TERMOMETER;
	}
	
	if (id.find("26-") !=  std::string::npos) {
		rc |= OW_HUMIDITY;
	}

	return rc;
}
