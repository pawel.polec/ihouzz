#include "nodeMqtt.h"
#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include <unistd.h>

#define mqtt_host "localhost"
#define mqtt_port 1883

static int run = 1;

void handle_signal(int s) {
	run = 0;
}

NodeMqtt::NodeMqtt (std::string id) {
	m_id = id;

	int rc = 0;
	readTopic = "";

	mosquitto_lib_init();

	mosq = mosquitto_new(id.c_str(), true, this);

	if ( mosq ) {
		mosquitto_message_callback_set(mosq, message_callback);

		rc = mosquitto_connect(mosq, mqtt_host, mqtt_port, 60);

		//mosquitto_subscribe(mosq, NULL, "/devices/wb-adc/controls/+", 0);
	}

	mosquitto_loop_start(  mosq  );
}

void NodeMqtt::setWriteTopic ( std::string topic ) {
	writeTopic = topic;
}

void NodeMqtt::setReadTopic ( std::string topic ) {
	if (readTopic != "") {
		mosquitto_unsubscribe(	mosq, NULL, readTopic.c_str());
	}
	readTopic = topic;
	if (readTopic != "") {
		mosquitto_subscribe(mosq, NULL, topic.c_str(), 0);
	}
}

float NodeMqtt::readValue () {
	return 0;
}

void NodeMqtt::message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message){
    NodeMqtt * self = static_cast<NodeMqtt*>(obj);
    self->callback(mosq, message);
}

void NodeMqtt::callback(struct mosquitto *mosq, const struct mosquitto_message *message){
    std::cout << message->topic << ": "<< (char*) message->payload << std::endl;
}

