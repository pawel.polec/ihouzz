#ifndef OBSERVER_H
#define OBSERVER_H

#include <iostream>
#include <vector>


class Observer {
public:
	virtual void update ( void * data ) = 0;
};


class Subject {
	std::vector<Observer*> views;

	public:

	void Register(class Observer* o);
	void Unregister(class Observer* o);
	void Notify();
};

int power(int i);

#endif //OBSERVER_H
