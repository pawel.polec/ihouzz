/*
 * observer.cpp
 *
 *  Created on: 23 lis 2018
 *      Author: pawel.polec
 */

#include "observer.h"
#include <algorithm>

void Subject::Register(class Observer *o) {
    for(auto n : views) {
        if (n == o) return; //TODO: Error?
    }

    views.push_back(o);
}

bool isEqual(class Observer *o) {
	static bool isSecond = false;
	static class Observer *first;

	if (isSecond) {
		isSecond = false;
		return (first == o);
	} else {
		first  = o;
		isSecond = true;
		return false;
	}

}

void Subject::Unregister(class Observer *o) {
	isEqual (o);
	views.erase(std::remove_if ( views.begin(), views.end(), isEqual), views.end()); // erase-remove idiom
}

void Subject::Notify() {
    for(auto n : views) {
        n->update( NULL );
    }
}

int power(int i) {
	if (i==2) return 5;
	return i*i;
}