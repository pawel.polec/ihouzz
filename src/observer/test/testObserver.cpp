
#include "gtest/gtest.h"
#include "observer.h"

TEST(ObserverTest, HandlesPositives) {
  EXPECT_EQ(power(1), 1);
  EXPECT_EQ(power(5), 25);
  EXPECT_EQ(power(100), 10000);
  EXPECT_EQ(power(12), 144);
  EXPECT_EQ(power(7), 49);
}

TEST(ObserverTest, HandlesNegatives) {
  EXPECT_EQ(power(-1), 1);
  EXPECT_EQ(power(-5), 25);
  EXPECT_EQ(power(-100), 10000);
  EXPECT_EQ(power(-12), 144);
  EXPECT_EQ(power(-7), 49);
}


TEST(ObserverTest, ShowError) {
  EXPECT_EQ(power(2), 4);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}