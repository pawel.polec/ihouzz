#ifndef SETTINGS_H
#define SETTINGS_H

#include "observer.h"
#include <thread>
#include <chrono>
#include <iostream>

enum eSettingsCell {
	SETTING_MODE,
	SETTING_SET_TEMP,
};

enum eCHCMode {
	CHC_MODE_AUTO,
	CHC_MODE_MANUAL,
};

class Settings : public Observer, public Subject {
	eSettingsCell settingType;
	union value {
		float temp;
		eCHCMode mode;
	};
public:

	Settings(eSettingsCell settingCell): settingType(settingCell) {
		//register on  DB subject
	}

	virtual ~Settings() {
		//unregister on  DB subject
	}

	virtual void update ( void * data );
};


#endif //SETTINGS_H
