#include "settings.h"

void Settings::update ( void * data ) {
	bool updated = false;
	switch (settingType) {
		case SETTING_MODE:
			//read from DB; if changed = updated = true;
			updated = true;
			break;

		case SETTING_SET_TEMP:
			//read from DB;
			updated = true;
			break;
	}

	if (updated) {
		Notify();
	}
}
